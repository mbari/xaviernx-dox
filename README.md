# Jetson Xavier NX ML-Tracking Setup

## Overview:
- Start with clean Jetpack
- Follow tensorbook folder structure
- Install dependencies for user in `~/.local`
- Create a Python3 virtual env called `ml-tracking` 
- Include system wide python3 site pkgs in the virtual env (use apt to install some pkgs that don't have arm support via pip)  


## Jetpack 4.4 Install

1. Create Jetpack SD card image following : https://developer.nvidia.com/embedded/learn/get-started-jetson-xavier-nx-devkit
6. Create user: John Q. Ops, username = ops, password ~ ask Paul or Mike


## Create Dev Directories

```
cd /home/ops
mkdir prj.local
cd prj.local
mkdir BioEng
```

## Add ~/.local to path
```
ops@ml-track:~$ echo "PATH=$HOME/.local/bin:$PATH" >> ~/.bashrc
ops@ml-track:~$ echo "PKG_CONFIG_PATH=$HOME/.local/lib/pkgconfig:$PKG_CONFIG_PATH" >> ~/.bashrc
```

Setup ssh keys
```
ops@ml-track:~$ mkdir .ssh
ops@ml-track:~$ ssh-keygen -t rsa -b 4096
# add the pub key to Bitbucket...
```


## Setup path for third-party software installs

```
ops@tensorbook1:~/prj.local/BioEng$ mkdir installed
ops@tensorbook1:~/prj.local/BioEng$ cd installed
```

## Install jetson stats
```
pip3 install -U setuptools pip
sudo pip3 install -U jetson-stats
sudo systemctl restart jetson_stats.service

# test 
sudo jtop
```

## Install python3 and qt5
```
sudo apt install qt5-default pyqt5-dev-tools qttools5-dev-tools
sudo apt install python3-dev python3-pip python3-venv python3-pyqt5 
```

## Setup python3 and virtual environments

```
sudo pip3 install virtualenv virtualenvwrapper
echo -e "\n# virtualenv and virtualenvwrapper" >> ~/.bashrc
echo "export WORKON_HOME=$HOME/.virtualenvs" >> ~/.bashrc
echo "export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3" >> ~/.bashrc
echo "source /usr/local/bin/virtualenvwrapper.sh" >> ~/.bashrc
source .bashrc
```

Now to actually create the virtualenv

```
ops@ml-track:~$ mkvirtualenv ml-tracking -p python3 --system-site-packages
```

Install typical python packages

```
(ml-tracking) ops@ml-track:~/prj.local/BioEng/installed$ pip -U pip setuptools
(ml-tracking) ops@ml-track:~/prj.local/BioEng/installed$ pip install numpy
(ml-tracking) ops@ml-track:~/prj.local/BioEng/installed$ pip install scipy
(ml-tracking) ops@ml-track:~/prj.local/BioEng/installed$ pip install pyqtgraph
(ml-tracking) ops@ml-track:~/prj.local/BioEng/installed$ pip install pyserial
(ml-tracking) ops@ml-track:~/prj.local/BioEng/installed$ pip install pyopengl
(ml-tracking) ops@ml-track:~/prj.local/BioEng/installed$ pip install matplotlib
(ml-tracking) ops@ml-track:~/prj.local/BioEng/installed$ pip install psutil
(ml-tracking) ops@ml-track:~/prj.local/BioEng/installed$ pip install openpiv
(ml-tracking) ops@ml-track:~/prj.local/BioEng/installed$ pip install pykalman
(ml-tracking) ops@ml-track:~/prj.local/BioEng/installed$ pip install imageio-ffmpeg
```

## Install openCV
```
(ml-tracking) ops@ml-track:~/prj.local/xaviernx-dox/build_opencv.sh
(ml-tracking) ops@ml-track:~$ ln -s ~/.local/lib/pkgconfig/opencv4.pc ~/.local/lib/pkgconfig/opencv.pc
(ml-tracking) ops@ml-track:~$ sudo bash -c 'echo /home/ops/.local/lib > /etc/ld.so.conf.d/opencv.conf'
(ml-tracking) ops@ml-track:~$ sudo ldconfig
```

## ML-Tracking Setup

### LCM Setup

```
ops@ml-track:~$ sudo apt-get install libglib2.0-dev
ops@ml-track:~$ sudo apt-get install default-jdk
ops@ml-track:~$ workon ml-tracking
(ml-tracking) ops@ml-track:~$ cd ~/prj.local/BioEng
(ml-tracking) ops@ml-track:~/prj.local/BioEng$ git clone https://github.com/lcm-proj/lcm.git
(ml-tracking) ops@ml-track:~/prj.local/BioEng$ mkdir lcm/build && cd lcm/build
(ml-tracking) ops@ml-track:~/prj.local/BioEng/lcm/build$ cmake cmake .. -DPYTHON_LIBRARY=/usr/lib/aarch64-linux-gnu/libpython3.6m.so -DPYTHON_INCLUDE_DIR=/usr/include/python3.6m -DPYTHON_EXECUTABLE=/home/ops/.virtualenv/ml-tracking/bin/python
(ml-tracking) ops@ml-track:~/prj.local/BioEng/lcm/build$ make -j6
(ml-tracking) ops@ml-track:~/prj.local/BioEng/lcm/build$ sudo make install
```

Set up Kernel UDP receive buffer size
```
sudo vim /etc/sysctl.conf
# and add tho following:

# Kernel UDP receive buffer sizing
net.core.rmem_max = 10485760
net.core.rmem_default = 10485760

# save and exit

sudo sysctl -p

```


Adding lcm to virtualenv install requires one more step

```
(ml-tracking) ops@ml-track:~/prj.local/BioEng/lcm/build$ cd ../lcm-python
(ml-tracking) ops@ml-track:~/prj.local/BioEng/lcm/lcm-python$ python setup.py install
```

Adding lcm to python 2.7 install as above

```
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/lcm/build$ deactivate
ops@tensorbook1:~/prj.local/BioEng/lcm/lcm-python$ sudo python setup.py install
```


Make sure to add the library path for LCM to 

```
ops@ml-track:~/prj.local/BioEng/lcm$ sudo bash -c 'echo /usr/local/lib > /etc/ld.so.conf.d/lcm.conf'
# or:
ops@ml-track:~/prj.local/BioEng/lcm/build$ sudo bash -c 'echo /home/ops/.local/lib > /etc/ld.so.conf.d/lcm.conf'

ops@ml-track:~/prj.local/BioEng/lcm/build$ sudo ldconfig
```
Add CLASSPATH
```
echo -e "\n# LCM spy type discover" >> ~/.bashrc
echo "export CLASSPATH=$CLASSPATH:/usr/local/share/java/lcmtypes_mwt.jar" >> ~/.bashrc
```

Test LCM

```
ops@ml-track:~/prj.local/BioEng/lcm/lcm-python$ cd ../examples/lcm-spy
ops@ml-track:~/prj.local/BioEng/lcm/examples/lcm-spy$ ./runspy.sh
```

Play a log file from one of the many MiniROV dives and confirm you see LCM messages in spy

### Docker and nvidia-docker setup: https://github.com/NVIDIA/nvidia-docker

### Install CVision AI images

`ops@tensorbook1:~$ docker pull cvisionai/mbari_tracking:2020.07.19`
`ops@tensorbook1:~$ docker pull cvisionai/mbari_tracking_test:2020.07.19`
`ops@tensorbook1:~$ docker pull cvisionai/lcm_image:2020.07.19`

### Install Vimba

Accept license and download Vimba 4.0 from here: https://www.alliedvision.com/en/products/software.html

Follow steps for installing Vimba under Linux here: https://cdn.alliedvision.com/fileadmin/content/documents/products/software/software/Vimba/appnote/Vimba_installation_under_Linux.pdf

Put Vimba in /home/ops/prj.local/BioEng/installed

Create a symlink to scripts:

`ops@tensorbook1:~/prj.local/BioEng/scripts$ ln -s ../installed/Vimba_4_0/Tools/Viewer/Bin/x86_64bit/VimbaViewer VimbaViewer`

### Download Python Camera Acquisition Software

```
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng$ pip install pymba
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng$ git clone https://pldr@bitbucket.org/mbari/mantaview.git
```

### Download MBARI ML-Tracking Software

```
ops@ml-track:~/prj.local/BioEng$ git clone git@bitbucket.org:mbari/mwt-control.git
ops@ml-track:~/prj.local/BioEng$ git clone git@bitbucket.org:mbari/mwt-apps.git
ops@ml-track:~/prj.local/BioEng$ git clone git@bitbucket.org:mbari/mwt-lcm-types.git
ops@ml-track:~/prj.local/BioEng$ git clone git@bitbucket.org:mbari/pcon-lib.git
ops@ml-track:~/prj.local/BioEng$ git clone git@bitbucket.org:mbari/ml-boxview.git
ops@ml-track:~/prj.local/BioEng$ git clone git@bitbucket.org:mbari/lcm-video-export.git
ops@ml-track:~/prj.local/BioEng$ git clone git@bitbucket.org:mbari/jetson-scripts.git scripts

```

### Download ML-Tracking Software Additional Libraries

```
(base) ops@tensorbook1:~/prj.local/BioEng$ git clone https://github.com/libbot2/libbot2.git

```


### Install MBARI ML-tracking Software

#### libbot2

`ops@ml-track:~/prj.local/BioEng$ cd libbot2`

Edit tobuild.txt to look like this:

```
# list of subdirectories to build, one one each line.  Empty lines
# and lines starting with '#' are ignored

bot2-core
#bot2-vis
bot2-procman
#bot2-lcmgl
bot2-lcm-utils
#bot2-param
#bot2-frames
```

Build the code and install in one command, otherwise install goes to the wrong place

`(ml-tracking) ops@ml-track:~/prj.local/BioEng/libbot2$ sudo make BUILD_PREFIX=/usr/local`


#### mwt-control

Note that mwt-control resuires pcon-lib to be at ../

```
sudo apt-get install libreadline-dev
ops@ml-track:~/prj.local/BioEng$ cd mwt-control
ops@ml-track:~/prj.local/BioEng/mwt-control$ make -j4
```

From what I can tell, the repo is missing the setup to build the gui. I copied this over
from rock-nuc-1 and it seems to work. We should update the repo to allow for building
the GUI with QtCreator

#### mwt-apps


Several changes were required to use mwt-apps with OpenCV 4. I created a new branch opencv4 in the repo and committed these changes


```
ops@ml-track:~/prj.local/BioEng$ cd mwt-apps
ops@ml-track:~/prj.local/BioEng/mwt-apps$ git checkout opencv4
ops@ml-track:~/prj.local/BioEng/mwt-apps$ make -j4
```
